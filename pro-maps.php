<?php
/*
Plugin Name: pro-maps
Plugin URI: http://test.com
Description: maps for shops
Version: 0.0.1
Author: Alex
Author URI: http://test.com
*/

if ( ! defined('ABSPATH')) {
	exit;
}

require_once( 'pro-maps-cpt.php' );
require_once( 'pro-maps-fields.php' );
require_once( 'pro-maps-scripts.php' );
require_once( 'pro-maps-front.php' );
require_once( 'pro-maps-shortcode.php' );
