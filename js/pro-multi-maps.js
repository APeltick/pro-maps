;(function() {

    var geocoder = new google.maps.Geocoder();
    var map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(27.68352808, 49.13085938),
        zoom: 2
    });

    for ( i = mapInfo.mag_amount; i >= 0; i-- ) {

        var address = mapInfo.mag_location[i];

        var content = mapInfo.mag_content[i];

        var infowindow = new google.maps.InfoWindow({
            content: content
        });

        geocoder.geocode( {'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });
                test = results[0].geometry.location;
            }
        });

    }

})();