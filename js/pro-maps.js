;(function() {
    var geocoder = new google.maps.Geocoder();
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12
    });
    var address = mapInfo.mag_location;

    var content = mapInfo.mag_content;

    var infowindow = new google.maps.InfoWindow({
        content: content
    });

    geocoder.geocode( {'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
            test = results[0].geometry.location;
        }
    });

})();