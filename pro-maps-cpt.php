<?php

add_action( 'init', 'register_post_type_pro_maps' );

add_action('init', 'register_taxonomy_pro_maps');


function register_post_type_pro_maps() {

	$singular = 'Pro Map';
	$plural = 'Pro Maps';

	$labels = array(
		'name'               => $plural,
		'singular_name'      => $singular,
		'add_new'            => 'Add new ' . $singular,
		'add_new_item'       => 'Add new ' . $singular . ' item',
		'edit_item'          => 'Edit ' . $singular,
		'new_item'           => 'New ' . $singular,
		'view_item'          => 'View ' . $singular,
		'search_items'       => 'Search ' . $plural,
		'not_found'          => $plural . ' not found',
		'not_found_in_trash' => $plural . ' not found in trash',
		'parent_item_colon'  => '',
		'menu_name'          => $plural,
	);

	$args = array(
			'labels'              => $labels,
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => null,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_rest'        => null,
			'rest_base'           => null,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-location-alt',
			'hierarchical'        => false,
			'supports'            => array('title','editor'),
			'taxonomies'          => array('pro_maps_tax'),
			'has_archive'         => true,
			'rewrite'             => true,
			'query_var'           => true,
	);

	register_post_type( 'pro_maps', $args );
}

function register_taxonomy_pro_maps() {

	$singular = 'Pro Map tax';
	$plural = 'Pro Maps taxs';

	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => 'Search ' . $plural,
		'all_items'         => 'All ' . $plural,
		'parent_item'       => null,
		'parent_item_colon' => null,
		'edit_item'         => 'Edit ' . $singular,
		'update_item'       => 'Update ' . $singular,
		'add_new_item'      => 'Add New ' . $singular,
		'new_item_name'     => 'New' . $singular . 'Name',
		'menu_name'         => $singular,
	);

	$args = array(
		'labels'                => $labels,
		'description'           => '',
		'public'                => true,
		'publicly_queryable'    => null,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_tagcloud'         => true,
		'show_in_rest'          => null,
		'rest_base'             => null,
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		'capabilities'          => array(),
		'meta_box_cb'           => null,
		'show_admin_column'     => false,
		'_builtin'              => false,
		'show_in_quick_edit'    => null,
	) ;

	register_taxonomy('pro_maps_tax', 'pro_maps', $args);
}

