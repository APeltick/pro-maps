<?php

add_action( 'wp_enqueue_scripts', 'add_map_scripts' );

function add_map_scripts() {
	wp_register_script( 'google_maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD4SKC7CqDbG_bAXLnjcGO4kRFxWN0fVMc', array('jquery') );
	wp_register_script( 'maps_js', plugins_url( 'js/pro-maps.js', __FILE__ ), array('google_maps') );
	wp_register_script( 'maps_multi_js', plugins_url( 'js/pro-multi-maps.js', __FILE__ ), array('google_maps') );
	wp_register_style( 'maps_css', plugins_url( 'css/pro-maps.css', __FILE__ ) );
}

