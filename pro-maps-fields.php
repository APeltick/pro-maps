<?php

add_action('add_meta_boxes', 'event_add_maps_metabox');

add_action('save_post', 'map_meta_save');

function event_add_maps_metabox() {

	add_meta_box(
		'pro_map_id',
		'Magazine address',
		'pro_map_callback',
		'pro_maps'
	);

}

function pro_map_callback( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'pro_map_nonce' );
	$pro_maps_meta = get_post_meta( $post->ID, 'pro_map', true );

	if ( ! empty ( $pro_maps_meta ) ) {
		$map = $pro_maps_meta;
	}
	?>
		<div>
			<div>
				<label for="pro-map"><?php _e( 'Magazine address' ) ?></label>
				<input type="text" name="pro_map" value="<?php _e( $map ) ?>" id="pro-map">
			</div>
		</div>
	<?php

}

function map_meta_save( $post_id ) {

	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'map_nonce' ] ) && wp_verify_nonce( $_POST[ 'map_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

	if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
		return;
	}

	if (isset( $_POST['pro_map'] ) ) {
		update_post_meta( $post_id, 'pro_map', $_POST['pro_map']);
	}

}