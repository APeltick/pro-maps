<?php


add_filter( 'the_content', 'add_map_to_content' );

function add_map_to_content( $content ) {

		if ( is_singular() && get_post_type() == 'pro_maps' ) {

			global $post;
			$mag_location = get_post_meta( $post->ID, 'pro_map', true );

			ob_start()
			?>
				<p><?php _e( the_title() ) ?></p>
				<p><?php _e( $mag_location ) ?></p>
			<?php
			$mag_content = ob_get_contents();
			ob_end_clean();

			wp_localize_script(
				'maps_js',
				'mapInfo',
				array(
					'mag_location' => $mag_location,
					'mag_content' => $mag_content
				)
			);

			$out = $content . '<div id="map"></div>';

			wp_enqueue_script( 'google_maps' );
			wp_enqueue_script( 'maps_js' );
			wp_enqueue_style( 'maps_css' );

			return $out;

		}

		return $content;

}