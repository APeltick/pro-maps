<?php


add_shortcode('promapshortcode', 'pro_maps_shortcode');

function pro_maps_shortcode() {


	global $post;
	$amount = 0;
	$content = [];
	$location = [];

	$maps = get_posts(
		array(
			'post_type'   => 'pro_maps',
			'order'       => 'DESC',
		)
	);

	foreach($maps as $post){ setup_postdata($post);
		$amount++;
		ob_start();
		?>
			<p><?php _e( $post->post_title ) ?></p>
			<p><?php _e( $post->post_content ) ?></p>
			<p><a href="<?php echo $post->guid ?>"><?php _e( 'Visit magazine page' ) ?></a></p>
		<?php
		$content[] = ob_get_contents();
		ob_end_clean();
		$location[] = $post->pro_map;
	}

	wp_reset_postdata ();

	wp_localize_script(
		'maps_multi_js',
		'mapInfo',
		array(
			'mag_amount' => $amount,
			'mag_content' => $content,
			'mag_location' => $location,
		)
	);

	wp_enqueue_script( 'google_maps' );
	wp_enqueue_script( 'maps_multi_js' );
	wp_enqueue_style( 'maps_css' );

	echo '<div id="map"></div>';
}